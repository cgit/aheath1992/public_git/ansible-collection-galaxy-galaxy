Name:		ansible-collection-galaxy-galaxy
Version:	2.0.3
Release:	1%{?dist}
Summary:	Ansible modules and plugins for working with Automation Hub

License:	GPL-3.0-only
URL:		%{ansible_collection_url ansible galaxy_collection}
Source0:	https://github.com/ansible/galaxy_collection/archive/%{version}/galaxy_collection-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	ansible-packaging

Patch0:         0001-Exclude-unnecessary-files-from-built-collection.patch

%description
ansible-collection-galaxy-galaxy provides the galaxy Ansible
collection. The collection includes Ansible modules and plugins for working
with Automation Hub.

%prep
%autosetup -n galaxy_collection-%{version} -p1
find -type f ! -executable -name '*.py' -print -exec sed -i -e '1{\@^#!.*@d}' '{}' +

%build
%ansible_collection_build

%install
%ansible_collection_install

%files -f %{ansible_collection_filelist}
%license COPYING
%doc README.md

%changelog
* Thu Sep 14 2023 Andrew H <aheath1992@gmail.com> - 2.0.3-1
- Inital Package
